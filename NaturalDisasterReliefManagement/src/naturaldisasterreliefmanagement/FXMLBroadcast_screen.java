/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaldisasterreliefmanagement;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class FXMLBroadcast_screen implements Initializable {

    @FXML
    private Button back_button;
    @FXML
    private Button logout_button;
    @FXML
    private ComboBox<String> template_dropdown;
    @FXML
    private ComboBox<String> disaster_dropdown;
    @FXML
    private TextArea broadcast_text_area;
    private ArrayList<DisasterText> disasterText;
    private List<String[]> lines;
    @FXML
    private ComboBox<String> state_dropdown;
    @FXML
    private ComboBox<String> county_dropdown;
    @FXML
    private Button broadcast_button;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        disasterText = getDisasters();
        disasterText.forEach((element) -> {
            disaster_dropdown.getItems().add(element.getDisasterType());
        });
        this.loadLocations();
        this.populateStates();
        this.initStateDropdown();
        this.initCountyDropdown();
    }

    private void initStateDropdown() {
        state_dropdown.editorProperty().getValue().textProperty().addListener(
                (v, oldVal, newVal) -> {
                    state_dropdown.show();
                    this.populateStates(newVal);
                    state_dropdown.setValue(newVal);
                    county_dropdown.setValue(null);
                }
        );
    }

    private void initCountyDropdown() {
        county_dropdown.editorProperty().getValue().textProperty().addListener(
                (v, oldVal, newVal) -> {
                    county_dropdown.show();
                    this.populateCounties(newVal);
                    county_dropdown.setValue(newVal);
                }
        );
    }

    @FXML
    private void backPressed(ActionEvent event) {
        Main.changeScene("main_screen.fxml");
    }

    @FXML
    private void broadcastPressed(ActionEvent event) {

        // Add to the front of the stack
        List<String> empties = new ArrayList();
        if (state_dropdown.getValue() == null || state_dropdown.getValue().isEmpty()) {
            empties.add("State");
        }
        if (county_dropdown.getValue() == null || state_dropdown.getValue().isEmpty()) {
            empties.add("County");
        }
        if (broadcast_text_area.getText().isEmpty()) {
            empties.add("Broadcast Message");
        }
        if (empties.size() > 0) {
            Alert warning = new Alert(AlertType.WARNING, "Please enter data in the follwoing field(s):\n" + empties.toString().replace("[", "").replace("]", ""));
            warning.setHeaderText(null);
            warning.initOwner(Main.stage);
            warning.showAndWait();
        } else {
            boolean confirmed = confirmation();
            if (confirmed) {
                addToStack();
            }
        }

    }

    private boolean confirmation() {
        Alert alert = new Alert(AlertType.CONFIRMATION, "Is the information below correct?\n"
                + "ADDRESS:\n"
                + this.county_dropdown.getValue() + " " + this.state_dropdown.getValue() + "\n\n"
                + "MESSAGE:\n"
                + this.broadcast_text_area.getText(), ButtonType.YES, ButtonType.NO);
        alert.setHeaderText(null);
        alert.initOwner(Main.stage);
        alert.showAndWait();
        if (alert.getResult().equals(ButtonType.YES)) {
            return true;
        }
        return false;
    }

    private void addToStack() {
        File file = new File("res/pastBroadcasts.txt");
        Scanner fileScan = null;
        try {
            fileScan = new Scanner(file);
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
        String broadcastInfo = "";
        while (fileScan.hasNextLine()) {
            broadcastInfo += fileScan.nextLine() + "\n";
        }
        fileScan.close();
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(file);
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d yyyy HH:mm:ss");
        pw.print(sdf.format(new Date()) + " - " + broadcast_text_area.getText() + "\n\n" + broadcastInfo + "\n");
        pw.close();
    }

    @FXML
    private void logoutPressed(ActionEvent event) {
        Main.changeScene("start_screen.fxml");
    }

    @FXML
    private void disasterAction(ActionEvent event) {
        String disasterType = disaster_dropdown.getSelectionModel().getSelectedItem();
        broadcast_text_area.clear();
        template_dropdown.getItems().clear();
        if (disasterType.equals("TEST") == false) {
            template_dropdown.disableProperty().set(false);
            template_dropdown.getItems().add(disasterText.get(disaster_dropdown.getSelectionModel().getSelectedIndex()).getDisasterType() + " - WATCH");
            template_dropdown.getItems().add(disasterText.get(disaster_dropdown.getSelectionModel().getSelectedIndex()).getDisasterType() + " - WARNING");
            return;
        }
        // Test announcement
        template_dropdown.disableProperty().set(true);
        broadcast_text_area.setText(disasterText.get(disaster_dropdown.getSelectionModel().getSelectedIndex()).getWatchText());
    }

    @FXML
    private void templateAction(ActionEvent event) {
        if (template_dropdown.getSelectionModel().getSelectedIndex() == 0) {
            broadcast_text_area.setText(disasterText.get(disaster_dropdown.getSelectionModel().getSelectedIndex()).getWatchText());
        } else {
            broadcast_text_area.setText(disasterText.get(disaster_dropdown.getSelectionModel().getSelectedIndex()).getWarningText());
        }
    }

    private ArrayList<DisasterText> getDisasters() {
        ArrayList<DisasterText> returnList = new ArrayList<>();
        File disasterFile = new File("res\\disaster.txt");
        try {
            Scanner txtParse = new Scanner(disasterFile);
            while (txtParse.hasNext()) {
                String disasterType = txtParse.nextLine();
                txtParse.nextLine();
                String watchText = txtParse.nextLine();
                txtParse.nextLine();
                returnList.add(new DisasterText(disasterType, txtParse.nextLine(), watchText));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Whoops, " + disasterFile.getPath() + " doesn't exist!");
        }

        return returnList;
    }

    private void loadLocations() {
        try {
            CsvParserSettings settings = new CsvParserSettings();
            settings.getFormat().setLineSeparator("\n");
            CsvParser parser = new CsvParser(settings);
            File file = new File("res/zip_codes_states.csv");
            this.lines = parser.parseAll(file);
            String[] header = this.lines.get(0); // Remove the header
            this.lines.remove(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void populateStates(String... filters) {
        String input = filters == null || filters.length == 0 ? "" : filters[0];
        List<String> states = new ArrayList();
        for (String[] line : this.lines) {
            String curr = line[4];
            if (states.contains(curr) == false) {
                if (input.length() == 0) {
                    states.add(curr);
                    continue;
                }
                if (curr.substring(0, input.length()).equalsIgnoreCase(input)) {
                    states.add(curr);
                }
            }
        }
        states.sort(String.CASE_INSENSITIVE_ORDER);
        ObservableList<String> obs_states = FXCollections.observableArrayList(states);
        this.state_dropdown.setItems(obs_states);
    }

    private void populateCounties(String... filters) {
        String input = filters == null || filters.length == 0 ? "" : filters[0];
        List<String> counties = new ArrayList();
        for (String[] line : this.lines) {
            String curr = line[5];
            String state = line[4];
            if (state.equalsIgnoreCase(state_dropdown.getValue()) && (counties.contains(curr) == false)) {
                if (input == null || input.length() == 0) {
                    counties.add(curr);
                    continue;
                }
                if (curr != null && curr.substring(0, input.length()).equalsIgnoreCase(input)) {
                    counties.add(curr);
                }
            }
        }
        try {
            counties.sort(String.CASE_INSENSITIVE_ORDER);
        } catch (Exception e) {
            // No idea why this works I kinda just threw it in there
            System.out.println(e.getMessage());
        }
        System.out.println(counties);
        ObservableList<String> obs_cities = FXCollections.observableArrayList(counties);
        this.county_dropdown.setItems(obs_cities);
    }

    @FXML
    private void stateSelected(ActionEvent event) {
        this.populateCounties();
        this.county_dropdown.hide();
    }

    @FXML
    private void countySelected(ActionEvent event) {
    }

}
