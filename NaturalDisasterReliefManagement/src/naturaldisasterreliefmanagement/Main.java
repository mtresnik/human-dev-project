/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaldisasterreliefmanagement;

import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Logan Stanfield
 * @author Mike Resnik
 * @author Thomas Pedraza
 */
public class Main extends Application {

    public static Stage stage;
    

    @Override
    public void start(Stage stage) throws Exception {
        Main.stage = stage;
        stage.setMinWidth(925);
        stage.setMinHeight(550);
        Main.changeScene("start_screen.fxml");
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            System.setProperty("glass.accessible.force", "false");
        launch(args);
        }catch(Exception e){
            System.out.println(e.getCause());
        }
    }

    public static void changeScene(String FXMLFile) {
        try {
            // Change root of scene so that there isn't weird fullscreen flickering issues.
            GridPane root = FXMLLoader.load(Main.class.getResource(FXMLFile));
            if(stage.isShowing() == false){
                Scene scene = new Scene(root);
                Main.stage.setScene(scene);
                Main.stage.setFullScreen(true);
                Main.stage.show();
            }
            Main.stage.getScene().rootProperty().setValue(root);
        } catch (IOException ioe) {
            System.err.println(ioe);
        }
    }
}
