/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaldisasterreliefmanagement;

import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author Mike
 */
public class FXMLDispatch_screen implements Initializable {

    @FXML
    private TextArea dispatch_text;
    @FXML
    private TextField address_text;
    @FXML
    private ComboBox<String> state_dropdown;
    @FXML
    private ComboBox<String> city_dropdown;
    @FXML
    private ComboBox<String> zip_dropdown;
    @FXML
    private CheckBox ems_checkbox;
    @FXML
    private CheckBox fire_checkbox;
    @FXML
    private CheckBox law_checkbox;

    private List<String[]> lines;
    @FXML
    private Button dispatch_button;

    // <editor-fold desc="Init">
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.loadLocations();
        this.populateStates();
        this.initStateDropdown();
        this.initCityDropdown();
        this.initZipDropdown();
    }

    private void initStateDropdown() {
        state_dropdown.editorProperty().getValue().textProperty().addListener(
                (v, oldVal, newVal) -> {
                    state_dropdown.show();
                    this.populateStates(newVal);
                    state_dropdown.setValue(newVal);
                    city_dropdown.setValue(null);
                    zip_dropdown.setValue(null);
                }
        );
        
    }

    private void initCityDropdown() {
        city_dropdown.editorProperty().getValue().textProperty().addListener(
                (v, oldVal, newVal) -> {
                    city_dropdown.show();
                    this.populateCities(newVal);
                    city_dropdown.setValue(newVal);
                    zip_dropdown.setValue(null);
                }
        );
        
    }

    private void initZipDropdown() {
        zip_dropdown.editorProperty().getValue().textProperty().addListener(
                (v, oldVal, newVal) -> {
                    zip_dropdown.show();
                    this.populateZipCodes(newVal);
                    zip_dropdown.setValue(newVal);
                }
        );
        
    }

    private void loadLocations() {
        try {
            CsvParserSettings settings = new CsvParserSettings();
            settings.getFormat().setLineSeparator("\n");
            CsvParser parser = new CsvParser(settings);
            File file = new File("res/zip_codes_states.csv");
            this.lines = parser.parseAll(file);
            String[] header = this.lines.get(0); // Remove the header
            this.lines.remove(0);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    //</editor-fold>

    // <editor-fold desc="Pressed">
    @FXML
    private void backPressed(ActionEvent event) {
        Main.changeScene("main_screen.fxml");
    }

    @FXML
    private void dispatchPressed(ActionEvent event) {

        List<String> failed = failedDispatch();
        if (failed.size() > 0) {
            String values = "";
            String first = failed.get(0);
            String firstChar = first.charAt(0) + "";
            first = firstChar.toUpperCase() + first.subSequence(1, first.length());
            values += first;
            failed.remove(0);
            values += (failed.isEmpty() ? "" : ", ") + failed.toString();
            String valString = values.replace('[', (char) 0).replace(']', (char) 0);
            Alert warning = new Alert(AlertType.WARNING, "Please enter data in the following field(s):\n" + valString);
            warning.setHeaderText(null);
            warning.initOwner(Main.stage);
            warning.showAndWait();
        }
        List<String> dispatchUnits = new ArrayList();
        if (ems_checkbox.isSelected()) {
            dispatchUnits.add("EMS");
        }
        if (fire_checkbox.isSelected()) {
            dispatchUnits.add("Fire Department");
        }
        if (law_checkbox.isSelected()) {
            dispatchUnits.add("Law Enforcement");
        }
        if (dispatchUnits.isEmpty()) {
            Alert warning = new Alert(AlertType.WARNING, "Please select atleast one dispatch unit\n (Ex: EMS, Fire Department, Law Enforcement)");
            warning.setHeaderText(null);
            warning.initOwner(Main.stage);
            warning.showAndWait();
        }
        if (failed.isEmpty() && (dispatchUnits.isEmpty() == false)) {
            Alert confirmation = new Alert(AlertType.CONFIRMATION,
                    "Is the information below correct?\n"
                    + "Address:\n".toUpperCase() + address_text.getText() + " "
                    + city_dropdown.getValue() + ", "
                    + state_dropdown.getValue() + " "
                    + zip_dropdown.getValue() + "\n\n"
                    + "Dispatch Message:\n".toUpperCase()
                    + dispatch_text.getText() + "\n\n"
                    + "Dispatch Units:\n".toUpperCase()
                    + dispatchUnits.toString(), ButtonType.YES, ButtonType.NO
            );
            confirmation.setHeaderText(null);
            confirmation.initOwner(Main.stage);
            confirmation.showAndWait();
            if (confirmation.getResult().equals(ButtonType.YES)) {
                // Dispatch Data
            }
        }
    }

    private List<String> failedDispatch() {
        List<String> retList = new ArrayList();

        if (address_text.getText().isEmpty()) {
            retList.add("address");
        }
        if (state_dropdown.getValue() == null || state_dropdown.getValue().isEmpty()) {
            retList.add("state");
        }
        if ((city_dropdown.getValue() == null || city_dropdown.getValue().isEmpty())) {
            retList.add("city");
        }
        if ((zip_dropdown.getValue() == null || zip_dropdown.getValue().isEmpty())) {
            retList.add("zip code");
        }
        if (dispatch_text.getText() == null || dispatch_text.getText().isEmpty()) {
            retList.add("dispatch message");
        }

        return retList;
    }

    @FXML
    private void logoutPressed(ActionEvent event) {
        Main.changeScene("start_screen.fxml");
    }

    @FXML
    private void stateKeyPressed(KeyEvent event) {
    }
    // </editor-fold>

    //<editor-fold desc="Checked">
    @FXML
    private void emsChecked(ActionEvent event) {
    }

    @FXML
    private void fireChecked(ActionEvent event) {
    }

    @FXML
    private void lawChecked(ActionEvent event) {
    }
    //</editor-fold>

    // <editor-fold desc="Selection">
    @FXML
    private void stateSelected(ActionEvent event) {
        this.city_dropdown.hide();
    }

    @FXML
    private void citySelected(ActionEvent event) {
        this.zip_dropdown.hide();
    }

    @FXML
    private void zipSelected(ActionEvent event) {
    }
    // </editor-fold>

    // <editor-fold desc="Population">
    private void populateStates(String... filters) {
        String input = filters == null || filters.length == 0 ? "" : filters[0];
        List<String> states = new ArrayList();
        for (String[] line : this.lines) {
            String curr = line[4];
            if (states.contains(curr) == false) {
                if (input.length() == 0) {
                    states.add(curr);
                    continue;
                }
                try{
                if (curr.substring(0, input.length()).equalsIgnoreCase(input)) {
                    states.add(curr);
                }
                }catch(Exception e){
                    
                }
            }
        }
        states.sort(String.CASE_INSENSITIVE_ORDER);
        ObservableList<String> obs_states = FXCollections.observableArrayList(states);
        this.state_dropdown.setItems(obs_states);

    }

    private void populateCities(String... filters) {
        String input = filters == null || filters.length == 0 ? "" : filters[0];
        List<String> cities = new ArrayList();
        for (String[] line : this.lines) {
            String curr = line[3];
            String state = line[4];
            if (state.equals(state_dropdown.getValue()) && (cities.contains(curr) == false)) {
                if (input.length() == 0) {
                    cities.add(curr);
                    continue;
                }
                try{
                if (curr.substring(0, input.length()).equalsIgnoreCase(input)) {
                    cities.add(curr);
                }
                }catch(Exception e){
                        
                }
            }
        }
        cities.sort(String.CASE_INSENSITIVE_ORDER);
        ObservableList<String> obs_cities = FXCollections.observableArrayList(cities);
        this.city_dropdown.setItems(obs_cities);
    }

    private void populateZipCodes(String... filters) {
        String input = filters == null || filters.length == 0 ? "" : filters[0];
        List<String> zips = new ArrayList();
        for (String[] line : this.lines) {
            String curr = line[0];
            String city = line[3];
            String state = line[4];
            if (city.equals(city_dropdown.getValue()) && state.equals(state_dropdown.getValue()) && (zips.contains(curr) == false)) {
                if (input.length() == 0) {
                    zips.add(curr);
                    continue;
                }
                try{
                if (curr.substring(0, input.length()).equalsIgnoreCase(input)) {
                    zips.add(curr);
                }
                }catch(Exception e){
                    
                }
            }
        }
        zips.sort(String.CASE_INSENSITIVE_ORDER);
        ObservableList<String> obs_zips = FXCollections.observableArrayList(zips);
        this.zip_dropdown.setItems(obs_zips);
    }

    // </editor-fold>
}
