package naturaldisasterreliefmanagement;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.geometry.*;

public class AlertBox {

    public static void display() {
        boolean loggedIn = false;
        Button loginButton;
        Stage window = new Stage();
        Scene scene;
        window.setTitle("NDRMS Login");
        //Form
        TextField username = new TextField();
        PasswordField password = new PasswordField();
        username.setPromptText("Username");
        password.setPromptText("Password");
        loginButton = new Button("Login");
        loginButton.setMaxWidth(261);
        loginButton.setMinWidth(261);
        loginButton.setOnAction(e -> {
            window.close();
        });

        //Layout
        VBox layout = new VBox(10);
        layout.setPadding(new Insets(20, 20, 20, 20));
        layout.getChildren().addAll(username, password, loginButton);

        scene = new Scene(layout, 300, 250);
        window.setScene(scene);
        window.show();
    }
}
