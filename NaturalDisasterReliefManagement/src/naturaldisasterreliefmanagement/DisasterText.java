/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaldisasterreliefmanagement;

/**
 * An object that holds information about a disaster. Broadcast_screen will
 * generate these and put them in a hashmap for the txtArea.
 *
 * @author Lunar
 */
public class DisasterText {

    private String disasterType;
    private String warningText;
    private String watchText;

    /*
    *@param disasterType The type of disaster this object represents.
    *@param warningText A string that should populate the broadcast text area when warning is selected.
    *@param watchText A string that should populate the broadcast text area when watch is selected.
     */
    public void DisasterText() {
        disasterType = "";
        warningText = "";
        watchText = "";
    }

    public DisasterText(String disasterType, String warningText, String watchText) {
        this.disasterType = disasterType;
        this.warningText = warningText;
        this.watchText = watchText;
    }

    public void setDisasterType(String disasterType) {
        this.disasterType = disasterType;
    }

    public void setWarningText(String warningText) {
        this.warningText = warningText;
    }

    public void setWatchText(String watchText) {
        this.watchText = watchText;
    }

    public String getDisasterType() {
        return disasterType;
    }

    public String getWarningText() {
        return warningText;
    }

    public String getWatchText() {
        return watchText;
    }

}
