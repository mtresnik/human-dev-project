/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaldisasterreliefmanagement;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Mike
 */
public class FXMLMain_screen implements Initializable {

    @FXML
    private Button logout_button;
    @FXML
    private Button natural_disaster_relief_button;
    @FXML
    private Button dispatch_button;
    @FXML
    private Button broadcast_button;
    @FXML
    private TextArea broadcast_display;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.initDisplay();
    }    

    
    private void initDisplay(){
        File file = new File("res/pastBroadcasts.txt");
        Scanner fileScan = null;
        try {
            fileScan = new Scanner(file);
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
        String broadcastInfo = "";
        while(fileScan.hasNextLine()){
            broadcastInfo += fileScan.nextLine() + "\n";
        }
        broadcast_display.setText(broadcastInfo);
    }
    
    @FXML
    private void logoutPressed(ActionEvent event) {
            Main.changeScene("start_screen.fxml");
    }


    @FXML
    private void dispatchPressed(ActionEvent event) {
            Main.changeScene("dispatch_screen.fxml");
    }

    @FXML
    private void broadcastPressed(ActionEvent event) {
            Main.changeScene("broadcast_screen.fxml");
    }

    @FXML
    private void naturalDisasterReliefPressed(ActionEvent event) {
        try{
            Desktop.getDesktop().browse(new URI("https://www.fema.gov/disasters"));
        }catch(IOException ioe){
            ioe.printStackTrace();
        }catch(URISyntaxException urise){
            urise.printStackTrace();
        }
    }
    
}
