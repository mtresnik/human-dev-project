/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaldisasterreliefmanagement;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 * FXML Controller class
 *
 * @author Mike
 */
public class FXMLStart_screen implements Initializable {

    @FXML
    private Button login_button;
    @FXML
    private Button natural_disaster_info_button;
    @FXML
    private TextArea broadcast_display;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.initDisplay();
        
    }   
    
    private void initDisplay(){
        File file = new File("res/pastBroadcasts.txt");
        Scanner fileScan = null;
        try {
            fileScan = new Scanner(file);
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
        String broadcastInfo = "";
        while(fileScan.hasNextLine()){
            broadcastInfo += fileScan.nextLine() + "\n";
        }
        broadcast_display.setText(broadcastInfo);
    }

    @FXML
    private void loginPressed(ActionEvent event) {
            Main.changeScene("login_screen.fxml");
    }

    @FXML
    private void naturalDisasterInfoPressed(ActionEvent event) {
        try{
            Desktop.getDesktop().browse(new URI("https://www.fema.gov/disasters"));
        }catch(IOException ioe){
            ioe.printStackTrace();
        }catch(URISyntaxException urise){
            urise.printStackTrace();
        }
    }
    
}
