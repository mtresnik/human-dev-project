/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package naturaldisasterreliefmanagement;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Mike
 */
public class FXMLLogin_screen implements Initializable {

    @FXML
    private TextField username_box;
    @FXML
    private Button login_button;
    @FXML
    private Button back_button;
    @FXML
    private PasswordField password_box;
    @FXML
    private Label error_label;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void loginPressed(ActionEvent event) {
        if(username_box.textProperty().getValueSafe().equalsIgnoreCase("username") && password_box.textProperty().getValueSafe().equalsIgnoreCase("password")){
            Main.changeScene("main_screen.fxml");
        }else{
            error_label.setText("Please enter a valid username and password combination.");
            username_box.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
            password_box.setStyle("-fx-border-color: red ; -fx-border-width: 2px ;");
        }
    }

    @FXML
    private void backPressed(ActionEvent event) {
        Main.changeScene("start_screen.fxml");
    }
    
}
